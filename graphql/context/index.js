const { User } = require("../../database/models");
const jwt = require("jsonwebtoken");
const { AuthenticationError } = require("apollo-server-express");

const verifyToken = async (token) => {
    try {
        if (!token) {
            // return new AuthenticationError("token needed.");
            return null;
        }
        const { id: tokenId } = jwt.verify(token, process.env.SECRET);
        const user = await User.findOne({ where: { id: tokenId } });
        if (!user) {
            throw new AuthenticationError("Unauthenticated")
        }
        return user;

    } catch (err) {
        throw err;
    }
}

module.exports = async ({ req }) => {
    const token = (req.headers && req.headers.authorization) || '';
    const user = await verifyToken(token);
    return { req, user };
}