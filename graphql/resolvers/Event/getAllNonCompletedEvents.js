const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");

const getAllNonCompletedEvents = async (parent, args, { user = null }) => {
  try {
    return await Event.findAll({ where: { completedAt: null }, include: ['creator'] });
  } catch (error) {
    throw error;
  }
}

module.exports = getAllNonCompletedEvents;