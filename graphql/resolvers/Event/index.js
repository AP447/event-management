const createEvent = require('./createEvent');
const getAllEvents = require('./getAllEvents');
const getAllCreatedEventsOfUser = require('./getAllCreatedEventsOfUser');
const deleteEvent = require('./deleteEvent');
const getAllNonCompletedEvents = require('./getAllNonCompletedEvents');
const getAllCompletedEvents = require('./getAllCompletedEvents');
const getAllEventsPagination = require('./getAllEventsPagination');
const updateEvent = require('./updateEvent');

module.exports = {
  Mutation: {
    createEvent,
    deleteEvent,
    updateEvent
  },
  Query: {
    getAllEvents,
    getAllCreatedEventsOfUser,
    getAllNonCompletedEvents,
    getAllCompletedEvents,
    getAllEventsPagination
  }
};