const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");

const getAllEvents = async(parent, args, context) => {
  try {
    return await Event.findAll();
  } catch (error) {
    throw error;    
  }
}

module.exports = getAllEvents;