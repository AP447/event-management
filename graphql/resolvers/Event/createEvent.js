const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");

const createEvent = async (_, args, { user = null }) => {
  try {
    const { eventName, description } = args.input;
    const userId = user.id;
    const eventDate = new Date();
    const eventInstance = { userId, eventName, description, eventDate };
    return await Event.create(eventInstance);
  } catch (error) {
    throw error;
  }
}

module.exports = createEvent;