const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");

const updateEvent = async (parent, args, { user = null }) => {
  try {
    const { oldEventName, newEventName, newDescription } = args.input;
    const eventCheck = await Event.findOne({ where: { eventName: oldEventName } });
    if (!eventCheck) {
      throw new AuthenticationError('event not found');
    }
    eventCheck.eventName = newEventName;
    eventCheck.description = newDescription;
    return await eventCheck.save();
  } catch (error) {
    throw error;
  }
}

module.exports = updateEvent;