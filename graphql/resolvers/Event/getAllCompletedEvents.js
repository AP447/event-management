const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const getAllCompletedEvents = async (parent, args, { user = null }) => {
  try {
    return await Event.findAll({
      where: {
        completedAt: {
          [Op.ne]: null
        }
      }, include: ['creator']
    });
  } catch (error) {
    throw error;
  }
}

module.exports = getAllCompletedEvents;