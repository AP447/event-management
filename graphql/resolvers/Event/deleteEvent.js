const { AuthenticationError } = require('apollo-server-express');
const { date } = require('joi');
const { Event } = require("../../../database/models");

const deleteEvent = async (_, args, { user = null }) => {
  try {
    const { eventId, eventName } = args.input;
    const checkEvent = await Event.findOne({ where: { id: eventId, userId: user.id } });
    if (!checkEvent) {
      return new AuthenticationError('Not Authorized');
    }
    checkEvent.deleteEventDate = new date();
    return await checkEvent.save();
  } catch (error) {
    throw error;
  }
}

module.exports = deleteEvent;