const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");

const getAllCreatedEventsOfUser = async (parent, args, { user = null }) => {
  try {
    return await Event.findAll({ where: { userId: user.id } });
  } catch (error) {
    throw error;
  }
}

module.exports = getAllCreatedEventsOfUser;