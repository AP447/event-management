const { AuthenticationError } = require('apollo-server-express');
const { Event } = require("../../../database/models");

const getAllEventsPagination = async (parent, args, context) => {
  const { limit, offset } = args.input;
  return await Event.findAll({ limit: limit, offset: offset});
}

module.exports = getAllEventsPagination;