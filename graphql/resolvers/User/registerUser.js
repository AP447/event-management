const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { AuthenticationError } = require('apollo-server-express');
const { User } = require("../../../database/models");

const createUser = async (parent, args, context) => {
  try {
    const { name, email, password } = args.input;
    const UserInstance = { name, email, password };
    const check = await User.findOne({ where: { email: email } });
    //check User is already available or not
    if (check) {
      throw new AuthenticationError("User already exists")
    }
    else {
      return await User.create(UserInstance);
    }
  }
  catch (error) {
    throw error;
  }
}

module.exports = createUser;