const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { AuthenticationError } = require('apollo-server-express');
const { User } = require("../../../database/models");

const login = async (parent, args, context) => {
  try {
    const { email, password } = args.input;
    const auth = await User.findOne({ where: { email: email } });
    if (!auth) {
      return new AuthenticationError('register first')
    }
    if (bcrypt.compareSync(password, auth.password)) {
      const token = jwt.sign({ id: auth.id }, process.env.SECRET);
      const user = auth.toJSON();
      return { ...user, token };
    } else {
      throw new AuthenticationError(' Invalid credentials');
    }
  } catch (error) {
    throw error;
  }
}

module.exports = login;