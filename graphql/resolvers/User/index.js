const registerUser = require('./registerUser');
const login = require('./login');
const changePassword = require('./changePassword');
const resetPassword = require('./resetPassword');

module.exports = {
  Mutation: {
    registerUser,
    login,
    changePassword,
    resetPassword
  },
}