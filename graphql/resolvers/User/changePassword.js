const { AuthenticationError } = require('apollo-server-express');
const { User } = require("../../../database/models");
const bcrypt = require('bcryptjs');

const changePassword = async (parent, args, { user = null }) => {
  try {
    const { oldPassword, newPassword, confirmNewPassword } = args.input;

    const userCheck = await User.findOne({ where: { id: user.id } });

    if (newPassword != confirmNewPassword) {
      return new AuthenticationError("newPassword and confirmNewPassword doesn't match.");
    }
    if (bcrypt.compareSync(oldPassword, userCheck.password)) {
      const newPass = await bcrypt.hash(newPassword, 10);
      user.password = newPass;
      return await user.save();
    } else {
      return new AuthenticationError("oldPassword doesn't matchMedia.");
    }
  } catch (error) {
    throw error;
  }
}

module.exports = changePassword;