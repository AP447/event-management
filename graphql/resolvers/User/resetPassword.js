const { AuthenticationError } = require('apollo-server-express');
const { User } = require("../../../database/models");
const bcrypt = require('bcryptjs');

const resetPassword = async (parent, args, { user = null }) => {
  const { newPassword, confirmNewPassword } = args.input;
  const userCheck = await User.findOne({ where: { email: user.email } });
  if (newPassword != confirmNewPassword) {
    throw new AuthenticationError("newPassword and confirmNewPassword doesn't match.");
  }
  const newPass = await bcrypt.hash(newPassword, 10);
  userCheck.password = newPass;
  console.log(userCheck);
  return await userCheck.save();
}

module.exports = resetPassword;