const { AuthenticationError } = require('apollo-server-express');
const { GuestList } = require("../../../database/models");

const getInvitedEventList = async (parent, args, { user = null }) => {
  try {
    console.log("user email------->", user.email);
    return await GuestList.findAll({ where: { email: user.email }, include: ['events'] });
    
  } catch (error) {
    throw error;
  }
}

module.exports = getInvitedEventList;