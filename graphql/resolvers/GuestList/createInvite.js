const { AuthenticationError } = require('apollo-server-express');
const { GuestList } = require("../../../database/models");
const { Event } = require("../../../database/models");


const createInvite = async (parent, args, { user = null }) => {
  const { eventId, email } = args.input;
  const eventCheck = Event.findOne({ where: { id: eventId, userId: user.id } });
  if (!eventCheck) {
    return new AuthenticationError('Not Authorized.');
  }
  const GuestListInstance = { eventId, email };
  return await GuestList.create(GuestListInstance);
}

module.exports = createInvite;