const createInvite = require('./createInvite');
const getInvitedEventList = require('./getInvitedEventList');

module.exports = {
  Mutation: {
    createInvite
  },
  Query: {
    getInvitedEventList
  }
}