const { gql } = require('apollo-server-express');

module.exports = gql `
  type user {
    id: Int,
    name: String,
    email: String,
  }

  extend type Mutation {
    registerUser(input: registerUserInput!): registerUserResponse,
    login(input: loginInput!): loginResponse,
    changePassword(input: changePasswordInput): changePasswordResponse,
    resetPassword(input: resetPasswordInput): changePasswordResponse
  }

  input resetPasswordInput {
    newPassword: String,
    confirmNewPassword: String
  }

  input registerUserInput {
    name: String,
    email: String,
    password: String
  }

  type registerUserResponse {
    id: Int,
    name: String,
  }

  input loginInput {
    email: String,
    password: String
  }

  type loginResponse {
    name: String,
    email: String,
    token: String,
  }

  input changePasswordInput {
    oldPassword: String,
    newPassword: String,
    confirmNewPassword: String
  }

  type changePasswordResponse {
    id: Int,
    name: String,
    email: String
  }
`
