const { gql } = require('apollo-server-express');

module.exports = gql`
  type event {
    id: Int,
    eventDate: Date,
    description: String,
    eventName: String,
    creator: user
  }

  extend type Mutation {
    createEvent(input: createEventInput): createEventResponse
    deleteEvent(input: deleteEventInput): deleteEventResponse
    updateEvent(input: updateEventInput): createEventResponse
  }

  extend type Query {
    getAllEvents: [event!]
    getAllCreatedEventsOfUser: [event!]
    getAllNonCompletedEvents: [event!]
    getAllCompletedEvents: [event!]
    getAllEventsPagination(input: getAllEventsPaginationInput): [event!]
  }

  input getAllEventsPaginationInput {
    offset: Int,
    limit: Int
  }

  input createEventInput {
    eventName: String,
    description: String,
    eventDate: Date,
  }

  type createEventResponse {
    eventName: String,
    description: String,
    eventDate: Date
  }

  input deleteEventInput {
    eventId: Int,
    eventName: String
  }

  type deleteEventResponse{
    id: Int
  }

  input updateEventInput {
    oldEventName: String,
    newEventName: String,
    newDescription: String
  }
`