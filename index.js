require('dotenv').config();

const server = require('./server/index');

server.listen(process.env.PORT, () => {
    console.log(`server created at ${process.env.PORT}`);
});